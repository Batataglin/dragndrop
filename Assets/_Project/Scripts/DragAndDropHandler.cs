using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public sealed class DragAndDropHandler : MonoBehaviour, IInitializePotentialDragHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public delegate void AudioHandler();
    public delegate void PlacementHandler();

    public event PlacementHandler OnPlaced;
    public event AudioHandler OnRightPlacement;
    public event AudioHandler OnWrongPlacement;

    [SerializeField] private Canvas canvas;
    [SerializeField] private float tweenDuration = 2f;

    private bool isPlaced;
    private Vector3 originPosition;

    private CanvasGroup canvasGroup;
    private RectTransform rectTransform;
    private RectTransform canvasRect;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvasRect = canvas.GetComponent<RectTransform>();
    }

    private void Start()
    {
        originPosition = rectTransform.position;
    }

    private void Update()
    {
        // Check for resolution changes
        if (Screen.width != canvasRect.rect.width || Screen.height != canvasRect.rect.height)
        {
            UpdateObjectPosition();
        }
    }

    #region Drag
    public void OnBeginDrag(PointerEventData eventData)
    {
        // Enable the raycast to go through the object
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!canvasGroup.interactable)
            return;

        // Use the pointer coordinates and divide by the canvas scale make sure the object is following the pointer
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!isPlaced)
            SetPosition(originPosition);
    }

    public void OnInitializePotentialDrag(PointerEventData eventData)
    {
        // Disable unity default drag threshold
        eventData.useDragThreshold = false;
    }
    #endregion

    public void SetPosition(Vector3 newPosition, bool _isPlaced = false)
    {
        canvasGroup.interactable = false;
        isPlaced = _isPlaced;

        // Raise the event to the Sound Manager
        if (isPlaced)
            OnRightPlacement?.Invoke();
        else
            OnWrongPlacement?.Invoke();

        // Tween the object position to it's destination. When complete enable the interaction;
        transform.DOMove(newPosition, tweenDuration).onComplete = EnableInteraction;
    }

    public void HandleGameRestart()
    {
        isPlaced = false;
        rectTransform.position = originPosition;
        EnableInteraction();
    }

    private void EnableInteraction()
    {
        // Doesn't toggle the interaction if the object is on place
        if (isPlaced)
        {
            // Raise the event to finish the game
            OnPlaced?.Invoke();
            return;
        }

        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
    }

    #region Keep the objects inside the screen

    private void UpdateObjectPosition()
    {
        // Calculate the object's position relative to the canvas
        Vector2 newAnchoredPosition = CalculateValidPosition(rectTransform.anchoredPosition);

        // Set the new position
        rectTransform.anchoredPosition = newAnchoredPosition;
    }

    private Vector2 CalculateValidPosition(Vector2 desiredPosition)
    {
        // Calculate the boundaries of the canvas
        Vector2 canvasSize = canvasRect.sizeDelta;
        Vector2 objectSize = rectTransform.sizeDelta;

        // Ensure the object stays within the canvas bounds
        float xMin = objectSize.x / 2;
        float xMax = canvasSize.x - objectSize.x / 2;
        float yMin = objectSize.y / 2;
        float yMax = canvasSize.y - objectSize.y / 2;

        // Clamp the desired position within the boundaries
        float x = Mathf.Clamp(desiredPosition.x, xMin, xMax);
        float y = Mathf.Clamp(desiredPosition.y, yMin, yMax);

        return new Vector2(x, y);
    }
    #endregion
}
