using UnityEngine;

public sealed class GameController : MonoBehaviour
{
    public delegate void GameHandler();

    public event GameHandler OnGameStart;
    public event GameHandler OnGameRestart;
    public event GameHandler OnGameFinish;

    public void HandleGameStart()
    {
        OnGameStart?.Invoke();
    }
    
    public void HandleGameRestart()
    {
        OnGameRestart?.Invoke();
    }

    public void HandleFinishGame()
    {
        OnGameFinish?.Invoke();
    }
}
