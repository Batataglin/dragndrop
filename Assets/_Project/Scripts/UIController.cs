using UnityEngine;
using UnityEngine.UI;

public sealed class UIController : MonoBehaviour
{
    public delegate void ResetGameHandler();

    public event ResetGameHandler OnResetGame;

    [SerializeField] private Button button;
    [SerializeField] private CanvasGroup canvasGroup;

    private void Awake()
    {
        button.onClick.AddListener(HandleGameReset);
    }

    public void HandleGameFinish()
    {
        canvasGroup.alpha = 1.0f;
        canvasGroup.interactable = true;
    }

    public void HandleGameStart()
    {
        canvasGroup.alpha = 0f;
        canvasGroup.interactable = false;
    }

    private void HandleGameReset()
    {
        HandleGameStart();
        OnResetGame?.Invoke();
    }
}
