using UnityEngine;
using UnityEngine.EventSystems;

public sealed class ItemSlot : MonoBehaviour, IDropHandler
{
    public delegate void ObjectDroppedHandler(Vector3 slotPosition, bool isPlaced);
    public event ObjectDroppedHandler OnDropped;

    private Vector3 slotPosition;

    private void Start()
    {
        slotPosition = GetComponent<RectTransform>().position;
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null )
        {
            OnDropped?.Invoke(slotPosition, true);
        }
    }
}
