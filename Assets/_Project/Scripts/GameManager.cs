using UnityEngine;

public sealed class GameManager : MonoBehaviour
{
    [SerializeField] private SoundController soundManager;
    [SerializeField] private GameController gameController;
    [SerializeField] private UIController uiController;
    [Space]
    [SerializeField] private ItemSlot itemSlot;
    [SerializeField] private DragAndDropHandler dragAndDropHandler;


    private void Awake()
    {
        gameController.OnGameStart += uiController.HandleGameStart;
        gameController.OnGameRestart += dragAndDropHandler.HandleGameRestart;
        gameController.OnGameFinish += uiController.HandleGameFinish;
        
        itemSlot.OnDropped += dragAndDropHandler.SetPosition;

        dragAndDropHandler.OnRightPlacement += soundManager.PlayRightPlacementAudio;
        dragAndDropHandler.OnWrongPlacement += soundManager.PlayWrongPlacementAudio;
        dragAndDropHandler.OnPlaced += gameController.HandleFinishGame;

        uiController.OnResetGame += gameController.HandleGameRestart;
    }

    private void Start()
    {
        // Start the game
        gameController.HandleGameStart();
    }
}
