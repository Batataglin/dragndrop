using UnityEngine;

public sealed class SoundController : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip rightPlacement;
    [SerializeField] private AudioClip wrongPlacement;

    public void PlayRightPlacementAudio()
    {
       _audioSource.PlayOneShot(rightPlacement);
    }

    public void PlayWrongPlacementAudio()
    {
       _audioSource.PlayOneShot(wrongPlacement);
    }
}
